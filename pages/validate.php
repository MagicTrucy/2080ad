<?php
$page_title="Validation de l'inscription";

include_once('../inc/sql.php');
$error_message = "";
if (!isset($_POST['email']))
	$_POST['email'] = "";
if (isset($_POST['email']) && isset($_POST['code_validation']))
{
	$result = query("SELECT COUNT(email) FROM user WHERE email=? AND token=?", array($_POST['email'], $_POST['code_validation']));
	if ($result[0][0] == "1")
	{
		query("UPDATE user SET valid_email=TRUE WHERE email=? AND token=?", array($_POST['email'], $_POST['code_validation']));
		header('Location: '.PAGE_ACCUEIL.'/pages/login.php?validated=1');
	}
	else
		$error_message = '<div class="alert alert-danger" role="alert" id="alerte_login_regex">Erreur lors de la validation : vérifiez votre adresse e-mail ainsi que le code de validation. En cas de problèmes, merci de contacter nyquist@2080ad.fr</div>';
}
include_once('../inc/header.php');
?>

Un e-mail a été envoyé à l'adresse e-mail que vous avez précisé. Il contient un code de validation, vous pouvez soit cliquer sur le lien de validation dans l'e-mail pour valider votre compte 2080 A.D.<br><br>
<?=$error_message?>
<div class="row">
	<div class="col-lg-4"></div>
	<div class="col-lg-4">
		<form role="form" method="POST" style="text-align:center">
			<div class="form-group">
				<input type="email" class="form-control" id="email" name="email" placeholder="Adresse e-mail" value="<?=$_POST['email']?>">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="code_validation" name="code_validation" placeholder="Code de validation">
			</div>
			<button type="submit" class="btn btn-default" name="send">Valider votre compte 2080 A.D.</button>
		</form>
	</div>
	<div class="col-lg-4"></div>
</div>



<?php
include_once('../inc/footer.php');
?>