<?php
$page_title="Connexion";
include_once('../inc/header.php');
$validated="";
if (isset($_GET['validated']))
    $validated = '<div class="alert alert-success" role="alert">Votre compte a été validé ! Vous pouvez vous identifier avec votre login et password ci-dessous :</div>';
if (isset($_POST['login']) && $_POST['login'] != "" && isset($_POST['password']) && $_POST['password'] != "")
{
    $correct_login = query("SELECT COUNT(login) from user WHERE login LIKE ? AND password LIKE ?", array($_POST['login'], crypt($_POST['password'])));
//    var_dump($correct_login);
}
?>

<?=$validated?>
<form role="form" method="POST">
    <div class="container">
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input type="text" class="form-control" id="login" name="login" placeholder="Login">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align:center">
        <button type="submit" value="connect" class="btn btn-primary">Se connecter</button>
        <a href="forgot_password.php" class="btn btn-danger">Mot de passe oublié</a>
        </div>
            <div class="col-md-4"></div>
    </div>
</form>

<?php
include_once('../inc/footer.php');
?>