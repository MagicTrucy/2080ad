<?php
$page_title="Mot de passe oublié";
include_once('../inc/header.php');
?>

<form role="form" method="POST">
<div class="alert alert-info" role="alert">Remplissez l'un des deux champs. Un lien permettant de générer un nouveau mot de passe vous sera envoyé à la dernière adresse e-mail connue. Attendez quelques minutes avant de réessayer, et pensez à vérifier votre dossier spam !
</div>
    <div class="container">
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input type="text" class="form-control" id="login" name="login" placeholder="Login">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input type="email" class="form-control" id="email" name="email" placeholder="Adresse e-mail">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align:center">
        <button type="submit" value="new_password" class="btn btn-primary">Envoyer</button>
        </div>
            <div class="col-md-4"></div>
    </div>
</form>

<?php
include_once('../inc/footer.php');
?>