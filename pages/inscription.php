<?php
$page_title="Inscription";
include_once('../inc/sql.php');
include_once('../inc/sanitization.php');
include_once('../inc/mail.php');
define('LOGIN_REGEX_ERROR', "Le login ne peut comporter que des lettres, chiffres, espaces (entre 2 mots seulement) et underscores.");
define('LOGIN_EXIST_ERROR', "Le login existe déjà.");
define('PASSWORD_SHORT_ERROR', "Le password doit comporter au moins 8 caractères.");
define('PASSWORD_SAME_ERROR', "Les mots de passe ne sont pas identiques.");
define('EMAIL_REGEX_ERROR', "L'adresse e-mail n'a pas le bon format.");
define('EMAIL_EXIST_ERROR', "Cette adresse e-mail est déjà utilisée.");
$error_message = "";
if(isset($_POST['send']))
{
    $good_to_go = true;
    if (isset($_POST['login']) && !valid_login($_POST['login']))
    {
        $error_message.=LOGIN_REGEX_ERROR."<br>";
        $good_to_go = false;
    }
    if (isset($_POST['login']) && query("SELECT COUNT(login) FROM user WHERE login=?", $_POST['login'])[0][0] != 0)
    {
        $error_message.=LOGIN_EXIST_ERROR."<br>";
        $good_to_go = false;
    }
    if (!isset($_POST['password']) || strlen($_POST['password']) < 8)
    {
        $error_message.=PASSWORD_SHORT_ERROR."<br>";
        $good_to_go = false;
    }
    if (isset($_POST['password']) && !is_null($_POST['password']) && !isset($_POST['confirmation_password']) || (isset($_POST['password']) && isset($_POST['confirmation_password']) && $_POST['password'] != $_POST['confirmation_password']))
    {
        $error_message.=PASSWORD_SAME_ERROR."<br>";
        $good_to_go = false;
    }
    if (isset($_POST['email']) && query("SELECT COUNT(email) FROM user WHERE email=?", $_POST['email'])[0][0] != 0)
    {
        $error_message.=EMAIL_EXIST_ERROR."<br>";
        $good_to_go = false;
    }
    if (!isset($_POST['email']) && !valid_email($_POST['email']))
    {
        $error_message.=EMAIL_REGEX_ERROR."<br>";
        $good_to_go = false;
    }
    if ($good_to_go)
    {
        $code_validation = substr(uniqid(), 0, 6);
        query("INSERT INTO user (login, password, statut, admin, email, token, mailing_list, valid_email) VALUES (?, ?, 0, 0, ?, ?, ?, FALSE)", array($_POST['login'], crypt($_POST['password']), $_POST['email'], $code_validation, $_POST['mailing_list'] == "true" ? TRUE : FALSE));
        sendmail($_POST['email'], "Validation de votre compte 2080 A.D.", "Bonjour ".$_POST['login'].",<br><br>Afin de valider votre compte 2080 A.D., merci de cliquer sur le lien suivant : <a href=\"".PAGE_ACCUEIL."/pages/validate.php?mail=".$_POST['email']."&code=".$code_validation."\">Valider mon compte</a> ou d'entrer le code ".$code_validation." sur <a href=\"".PAGE_ACCUEIL."/pages/validate.php\">".PAGE_ACCUEIL."/pages/validate.php</a> ainsi que votre adresse e-mail.<br>
            Si vous n'êtes pas à l'origine de cette demande, ignorez simplement cet e-mail.<br>
            N'hésitez pas à envoyer un mail à nyquist@2080ad.fr en cas de problèmes.<br>
            À bientôt sur le site,<br><br>
            L'équipe 2080 A.D.<br><br><br>
            Ceci est un e-mail automatique, merci de ne pas répondre à cet e-mail, votre réponse ne sera pas lue.");
        header("Location: ".PAGE_ACCUEIL."/pages/validate.php?email=".$_POST['email']);
    }
}
include_once('../inc/header.php');
?>

<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h2>ATTENTION</h2><br>
    Ce site est réservé aux joueurs ou MJ de 2080 A.D. Si vous n'êtes ni l'un, ni l'autre, votre compte ne sera pas reconnu
    et vous ne pourrez rien faire.<br>
    Par contre, vous pouvez envoyer un mail à shindranel@2080ad.fr pour prendre contact avec moi et ainsi vous présenter plus en profondeur 2080 A.D.
    Si vous êtes un joueur ou MJ, votre compte devra être validé avant de pouvoir interagir avec le site.
</div>
<div class="alert alert-info" role="alert">Tous les champs doivent être remplis. Votre adresse e-mail ne sera utilisée que par l'équipe de 2080 A.D. et ne sera jamais divulguée à l'extérieur.<br><?=$error_message?></div>

<form role="form" method="POST">
    <div class="form-group">
        <label for="login">Login</label>
        <div class="alert alert-danger" role="alert" id="alerte_login_regex" style="display:none"><?=LOGIN_REGEX_ERROR?></div>
        <div class="alert alert-danger" role="alert" id="alerte_login_exist" style="display:none"><?=LOGIN_EXIST_ERROR?></div>
        <input type="text" class="form-control" id="login" name="login" placeholder="Le login n'est pas votre nom de personnage" onblur="checkLogin()">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <div class="alert alert-danger" role="alert" id="alerte_password_short" style="display:none"><?=PASSWORD_SHORT_ERROR?></div>
        <input type="password" class="form-control" id="password" name="password" placeholder="Le mot de passe doit faire au moins 8 caractères" onblur="checkPasswordLength()">
    </div>
    <div class="form-group">
        <label for="confirmation_password">Confirmation du password</label>
        <div class="alert alert-danger" role="alert" id="alerte_password" style="display:none"><?=PASSWORD_SAME_ERROR?></div>
        <input type="password" class="form-control" id="confirmation_password" name="confirmation_password" placeholder="Retapez le même mot de passe" onblur="checkPasswords()">
    </div>
    <div class="form-group">
        <label for="email">Adresse e-mail</label>
        <div class="alert alert-danger" role="alert" id="alerte_email_regex" style="display:none"><?=EMAIL_REGEX_ERROR?></div>
        <div class="alert alert-danger" role="alert" id="alerte_email_exist" style="display:none"><?=EMAIL_EXIST_ERROR?></div>
        <input type="email" class="form-control" id="email" name="email" placeholder="Une vérification sera faite" onblur="checkMail()">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="mailing_list" checked value="true"> Cochez cette case pour recevoir les mises à jour du jeu
        </label>
    </div>
    <button type="submit" class="btn btn-default" name="send">Créer votre compte 2080 A.D.</button>
</form>

<script type="text/javascript">
    function checkLogin(){
        var login = document.getElementById('login').value;
        var login_regex = false;
        var login_short = false;
        document.getElementById('alerte_login_exist').style.display = "none";
        if (/^[a-zA-Z0-9_](\ ?[a-zA-Z0-9_]+)*$/.test(login))
            login_regex = true;
        if (login.length >= 2)
            login_short = true;
        if (/^[a-zA-Z0-9_](\ ?[a-zA-Z0-9_]+)*$/.test(login) && login.length >= 2)
            jQuery.ajax({
                url:'../inc/ajax/check_login.php?login='+login,
                complete:function(responseText){
                    if (responseText.responseText != 0)
                        document.getElementById('alerte_login_exist').style.display = "block";
                }
            });
        document.getElementById('alerte_login_regex').style.display = login_regex ? "none" : "block";
        document.getElementById('alerte_login_short').style.display = login_short ? "none" : "block";
    }
    function checkPasswordLength(){
        if (document.getElementById('password').value.length < 8)
            document.getElementById('alerte_password_short').style.display = "block";
        else
            document.getElementById('alerte_password_short').style.display = "none";
    }
    function checkPasswords(){
        var password = document.getElementById('password').value;
        var password_check = document.getElementById('confirmation_password').value;
        if (password!=password_check)
            document.getElementById('alerte_password').style.display = "block";
        else
            document.getElementById('alerte_password').style.display = "none";
    }
    function checkMail(){
        var email = document.getElementById('email').value;
        if (!email.match(/([a-z]|\.|_|[0-9])+@([a-z]|\.|_|[0-9])+\.([a-z]){2,}/i))
            document.getElementById('alerte_email_regex').style.display = "block";
        else
            jQuery.ajax({
                url:'../inc/ajax/check_email.php?email='+email,
                complete:function(responseText){
                    if (responseText.responseText != 0)
                        document.getElementById('alerte_email_exist').style.display = "block";
                    else
                        document.getElementById('alerte_email_exist').style.display = "none";
                }
            });   
    }
</script>
<?php
include_once('../inc/footer.php');
?>
