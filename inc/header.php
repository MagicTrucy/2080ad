<?php

include_once('conf.php');

?>

<!-- HEADER -->
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?=PAGE_ACCUEIL?>/css/bootstrap.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<title>2080 A.D. — <?=$page_title?></title>
</head>
<body>

<div class="navbar navbar-default navbar-inverse" role="navigation" style="border-radius:0px">
	<div class="container">
		<div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-navbar">
        <span class="sr-only">Menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
		  <a class="navbar-brand" href="<?=PAGE_ACCUEIL?>">2080 A.D.</a>
		</div>
		<div class="collapse navbar-collapse" id="collapse-navbar">
		  <ul class="nav navbar-nav">
        <li><a href="<?=PAGE_ACCUEIL?>/pages/inscription.php">Inscription</a></li>
        <li><a href="<?=PAGE_ACCUEIL?>/pages/login.php">Connexion</a></li>
        <li><a href="http://forum.2080ad.fr">Forum</a></li>
		  </ul>
		</div>
	</div>
</div>
<!-- !HEADER -->
<div class="container">
  <div class="page-header">
    <h1 style="text-align:center"><?=$page_title?></h1>
  </div>