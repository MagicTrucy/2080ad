<?php

function valid_login($login)
{
	return preg_match('/^[a-zA-Z0-9_](\ ?[a-zA-Z0-9_]+)*$/', $login);
}

function valid_email($email)
{
	return preg_match("/([a-zA-Z]|\.|_|[0-9])+@([a-zA-Z]|\.|_|[0-9])+\.([a-zA-Z]){2,}$/", $email);
}

?>