<?php

include_once('conf.php');
include_once('sanitization.php');

$dbh = null;

try
{
    $dbh = new PDO('mysql:host=localhost;dbname='.USE_DB, USER_DB, USER_PWD);
}
catch (PDOException $e)
{
    print utf8_decode("Erreur ! Merci d'envoyer un mail à nyquist@2080ad.fr avec ce message et comment vous l'avez rencontré : ".$e->getMessage()."<br/>");
    die();
}

function query($query, $parameters)
{
    global $dbh;
	$prepared_query = $dbh->prepare($query);
	try
	{
        if (!is_array($parameters))
            $parameters = array($parameters);
		$result = $prepared_query->execute($parameters);
	}
	catch (PDOException $e)
	{
    	print utf8_decode("Erreur ! Merci d'envoyer un mail à nyquist@2080ad.fr avec ce message et comment vous l'avez rencontré : ".$e->getMessage()."<br/>");
    	die();
	}
	return $prepared_query->fetchAll();
}

// Returns true if the login doesn't exist
function checkLogin($login)
{
	if (!is_null($login) && valid_login($login))
		query("SELECT COUNT(login) FROM user WHERE login=?", $login);
}

// Return true if the mail doesn't exist
function checkMail($mail)
{

}

?>
